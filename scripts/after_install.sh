#!/bin/bash
cp /home/ubuntu/project_worc/scripts/worc /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/worc /etc/nginx/sites-enabled
systemctl restart nginx

myprocs=`ps aux |grep gunicorn |grep worc | awk '{ print $2 }'`
cd /home/ubuntu/project_worc/server

if ! [ -z "$myprocs" ];then
	ps aux |grep gunicorn |grep worc | awk '{ print $2 }'| xargs kill -HUP	
fi

if [ -f "/home/ubuntu/project_worc_bkp/worc.db" ]; then
	cp /home/ubuntu/project_worc_bkp/worc.db /home/ubuntu/project_worc/server/db
fi

sudo chown -R ubuntu:ubuntu /home/ubuntu/project_worc/