## Desafio Worc
### Desenvolvimento de APIs
```
/candidates/
/candidates/1/
/contacts/
/contacts/1/
```
### Ambientes e tecnologias
* Ubuntu
* Vue.JS
* Python flask
* SQLite

## Instalação (DEV)

###  1) Instalação e utilização do client
> Será necessário baixar o npm e através dele, o Vue. Após a instalação, o comando #4 abaixo irá instalar todas as dependências deste sistema
> O arquivo onde a implementação principal se encontra está em: client/src/components/Home.vue

```
1. sudo apt-get install npm
2. Ir pasta client 
3. npm install vue
4. npm install
5. npm run serve 
```

### 2) Instalação e utilização do server
> Será necessário baixar uma versão recente do Python e o venv. 
> O arquivo onde a implementação das APIs se encontra está em: server/worc.py

```
1. sudo apt-get install python3.8
2. sudo apt-get install python3-venv
3. Ir pasta server
4. python3.8 -m venv env
5. source env/bin/activate
6. pip3 install -r requirements.txt
7. python3.8 worc.py
```

### 3) Acesso
> O sistema estará disponível para acesso via web através do link http://localhost:8080.

### 4) Instalação do SGBD
> Para persistir os dados, foi utilizado o Sistema Gerenciador de Banco de Dados Relacional (SGBDR) SQLITE, por ser simples de instalar e manter porém capaz de realizar as requisições dessa  atividade.
> O arquivo onde e a implementação da camada de persistência se encontra está em : server/db.py.
> Ao realizar a primeira requisição de qualquer uma das APIs, arquivo do banco de dados será criado pela função create_database() na pasta server/db . Os scripts de criação das tabelas se encontram na pasta server/db/scripts.
> O nome das tabelas e dos atributos seguiu o modelo apresentado abaixo. Assim, existe uma tabela CANDIDATES e uma tabela CONTACTS, o relacionamento entre as tabels é 1:N da CANDIDATES para a CONTACTS.

```
{
	"name": "Fabricio",
	"sobrenome": "Pereira",
	"document": "28.718.220/0001-74",
	"type_document": "CNPJ",
	"job": "PO",
	"contacts": [
		"type": "comercial",
		"number": "5516996112618"
	]
}
```

## Prova de conceito (PROD)

> É possível acessar uma versão funcional do sistema através do link http://18.219.33.254:8081/. Este link está hospedado em uma máquina EC2 da AWS. Através dos arquivos bitbucket-pipelines.yml e appspec.yml e também dos scripts na pasta script, ao realizar um git push na branch master, o pipeline de CD/CI é inicialiazado e a publicação do sistema é feita no link apresentado.

> O nginx é responsável por disponibilizar toda a estrutura do front-end (FE) na porta 8081. O FE faz requisições para as APIs do Flask - que é mantido pelo servidor do Gunicorn, este se comunica com o SQLITE para persistênciados dados.

## Casos de Uso

### (Samuca) Atualizar o nome, sobrenome e contato dos candidatos.
> Para atualizar o nome, sobrenome e os contatos, utiliza-se a API /candidates/<id_candadidate> com o método PUT que irá chamar a função para atualizar o candidato. A API está implementada no arquivo worc.py a partir da linha 36. Visualmente, o usuario deve clicar em no botão 'Editar', será aberta uma janela para alterar os dados do candidate. Para salvar utiliza-se o botão 'Salvar' e todos os dados serão salvos. 

> Para inserir ou atualizar somente o contato (individualmente) utiliza-se as APIs /contacts/ com o método POST e /contacts/<id_contacts> com o método PUT, respectivamente, que irão chamar as funções apropriadas. Essas APIs estão implementadas no arquivo worc.py a partir das linhas 59 e 75, respectivamente. Visualmente, nesta janela de edição de candidato, utiliza-se o botão 'Salvar' ao lado de cada contato para salvá-lo individualmente (se o contato existir, será atuaizado e se não existir será criado).

### (Alfi) Adicionar candidatos (candidatos não podem ser repetidos).
> Para adicionar um candidato, utiliza-se a API /candidates com o método POST que irá chamar a função para criar um candidato e também os contatos. A API está implementada no arquivo worc.py a partir da linha 17. A restrição de não duplicidade é garantida pelo banco de dados com o atributo UNIQUE no campo DOCUMENT (CPF ou CNPJ). Ao tentar realizar a inserção de um candidato que já existe, a API retornará uma mensagem de erro. Visualmente, para criar o candidato, o usuário deve clicar no botão 'Adicionar Candidate'. Será aberta uma janela para preenchimento do formulário. Nesta janela também é possivel inserir os contatos clicando no botão 'Adicionar Contact'. Ao clicar em 'Salvar' os dados serão persistidos e a janela fechará automaticamente. Uma mensagem será apresentada ao usuário para informar se o usuário foi inserido com sucesso ou não (por exemplo em caso de duplicidade). 

### (Fabricio) Ao criar um candidato, verificar que existe novos candidatos e saber se o usuario atualizou os contatos.
> Para atender a esse pedido, sempre que é realizado a adição, remoção ou atualização de um novo candidato o FE faz uma requisição para a API /candidates com o método GET que trará todos os candidatos da base. A API está implementada no arquivo worc.py a partir da linha 17. E para mostrar que os contatos foram atualizados, a API /candidates no método GET traz para cada candidato um atributo 'atualizacao' que pega a mudança mais recente de um contato - isso pode ser verificado na classe db.py a partir da linha 59. Para atualizar o atributo 'atualizacao' o a API /contacts/<id_contacts> no método PUT atualiza também este atributo.

### APIs

| API | Metodo | Função |
| --- | --- | --- |
| /candidates | GET | Retorna todos os candidatos |
| /candidates | POST | Adiciona um candidato |
| /candidates/<id> | PUT | Edita um candaidato |
| /candidates/<id> | DELETE | Remove um candidato |
| /contacts | GET | Retorna todos os contatos |
| /contacts | POST | Adiciona um contato |
| /contacts/<id> | PUT | Edita um contato |
| /contacts/<id> | DELETE | Remove um contato |