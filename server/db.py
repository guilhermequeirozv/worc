import sqlite3
from sqlite3 import Error
import os

def create_database():
	try:
		dbf = open('./db/worc.db','w')
		dbf.close()
		conn = sqlite3.connect('./db/worc.db')
		cur = conn.cursor()
		sqlfile = open("./db/scripts/create_db.sql")
		sqlfile_str = sqlfile.read()
		cur.executescript(sqlfile_str)
		sqlfile.close()
		conn.commit
		cur.close()
		return 1
	except Exception as e:
		print(e)
		os.remove('./db/worc.db')
		return str(e)

def cdu_dbobject(dbobject, sql, custom_processing):
    try:
        if (os.path.isfile('./db/worc.db')) is False:
        	create_database()
        conn = sqlite3.connect('./db/worc.db')
        cur = conn.cursor()
        cur.execute(sql, dbobject)
        dboject = custom_processing(cur,dbobject)
        conn.commit()
        cur.close()
        return dbobject
    except Exception as e:
        print(e)
        return str(e)


def s_dbobject(sql, filter, index):
    try:
        if (os.path.isfile('./db/worc.db')) is False:
        	if create_database() == 0:
        		return None
        conn = sqlite3.connect('./db/worc.db')
        cur = conn.cursor()
        cur.execute(sql, filter)
        dbobjects = {}
        columns = list(map(lambda x: x[0], cur.description))
        for row in cur:
            dbobject = {}
            for i in range(len(row)):
                dbobject[columns[i]] = row[i]
            dbobjects[row[index]] = dbobject
        return dbobjects
    except Exception as e:
        print(e)
        return None

def select_candidates():
    sql = ''' select id,name,sobrenome,document,type_document,job,contacts_str, atualizacao
			  from CANDIDATES ca left join (
				select candidate_id,GROUP_CONCAT(number||' '|| type,';') as contacts_str,
					max(system_update_date) as atualizacao
				from contacts group by candidate_id
			  ) co on
			  co.candidate_id = ca.id '''
    result = s_dbobject(sql, {}, 0)
    for candidate in result:
        result[candidate]['contacts'] = select_contact_byCandidate(result[candidate])
    return result

def returnObjectWithID(cursor,db_object):
    db_object['id'] = cursor.lastrowid
    return db_object

def default_custom_processing(cursor,db_object):
    return db_object


def insert_candidate(candidate):
    sql = ''' INSERT INTO CANDIDATES(NAME, SOBRENOME, DOCUMENT, TYPE_DOCUMENT, JOB, system_creation_date)
              VALUES(:name,:sobrenome,:document,:type_document,:job, date('now') || ' ' || time('now'))  
          '''
    result = cdu_dbobject(candidate,sql,returnObjectWithID)
    if isinstance(result, str):
        return {'status':'failure','message': 'Erro ao inserir candidate. Verifique se o candidate já existe'}
    contact_msg = ' '
    for contact in candidate['contacts']:
        contact_obj = candidate['contacts'][contact]
        contact_obj['candidate_id'] = candidate['id']
        result_contact = insert_contact(contact_obj)
        if result_contact['status']=='failure':
            contact_msg = contact_msg + ' contact {0} falhou.'.format(contact_obj['number'])
    return {'status': 'success','message': 'Candidate inserido.{0}'.format(contact_msg)}

def update_candidate(candidate):
    sql = ''' UPDATE CANDIDATES SET NAME = :name, SOBRENOME = :sobrenome, JOB = :job,
              system_update_date = date('now') || ' ' || time('now') WHERE 
              id = :id'''    
    result = cdu_dbobject(candidate,sql,default_custom_processing)
    existing_contacts = select_contact_byCandidate(candidate)  
    contact_msg = ''
    for contact in candidate['contacts']:
        saved_contact = existing_contacts.get(int(contact))
        if saved_contact is not None and (saved_contact['type'] != candidate['contacts'][contact]['type'] or saved_contact['number'] != candidate['contacts'][contact]['number']): 
            contact_obj = candidate['contacts'][contact]
            result_contact = update_contact(contact_obj)
            if result_contact['status']=='failure':
                contact_msg = contact_msg + ' contact {0} falhou.'.format(contact_obj['number'])
        elif saved_contact is None:#if it does not, insert
            contact_obj = candidate['contacts'][contact]
            result_contact = insert_contact(contact_obj)
            if result_contact['status']=='failure':
                contact_msg = contact_msg + ' contact {0} falhou.'.format(contact_obj['number'])
    return {'status': 'success','message': 'Candidate Atualizado.{0}'.format(contact_msg)}

def delete_candidate(candidate):
    sql = ''' DELETE FROM CANDIDATES WHERE id = :id '''
    result = cdu_dbobject(candidate,sql,default_custom_processing)
    if isinstance(result, str):
        return {'status':'failure','message': 'Erro ao deletar candidate.'}
    return {'status':'success','message': 'Candidate removido.'}

def select_candidate(candidate):
    sql = ''' select id,name,sobrenome,document,type_document,job,contacts_str, atualizacao
			  from CANDIDATES ca left join (
				select candidate_id,GROUP_CONCAT(number||' '|| type,';') as contacts_str,
					max(system_update_date) as atualizacao
				from contacts group by candidate_id
			  ) co on
			  co.candidate_id = ca.id where id = :id '''
    result = s_dbobject(sql, candidate, 0)
    if candidate['id'] in result:
        result[candidate['id']]['contacts'] = select_contact_byCandidate(result[candidate['id']])
    return result

def select_contacts():
    sql = ''' SELECT ID, CANDIDATE_ID, TYPE, NUMBER
              FROM CONTACTS'''
    result = s_dbobject(sql, {}, 0)
    return result

def insert_contact(contact):
    sql = ''' INSERT INTO CONTACTS(CANDIDATE_ID, TYPE, NUMBER, system_creation_date)
              VALUES(:candidate_id,:type,:number,date('now') || ' ' || time('now')) '''
    result = cdu_dbobject(contact,sql,returnObjectWithID)
    if isinstance(result, str):
        return {'status':'failure','message': result}
    return {'status': 'success', 'message': 'Contact inserido.'}

def update_contact(contact):
    sql = ''' UPDATE contacts SET type = :type, number = :number,
              system_update_date = date('now') || ' ' || time('now') WHERE 
              id = :id'''
    result = cdu_dbobject(contact,sql,default_custom_processing)
    if isinstance(result, str):
        return {'status':'failure','message': result}
    return {'status': 'success', 'contact': 'Contact atualizado.'}

def delete_contact(contact):
    sql = ''' DELETE FROM contacts WHERE id = :id '''
    result = cdu_dbobject(contact,sql,default_custom_processing)
    if isinstance(result, str):
        return {'status':'failure','message': result}
    return {'status': 'success', 'contact': 'Contact removido.'}

def select_contact(contact):
    sql = ''' SELECT ID, CANDIDATE_ID, TYPE, NUMBER 
              FROM contacts WHERE id = :id '''
    return s_dbobject(sql, contact, 0)

def select_contact_byCandidate(candidate):
    sql = ''' SELECT ID, CANDIDATE_ID, TYPE, NUMBER 
              FROM contacts WHERE candidate_id = :id '''
    return s_dbobject(sql, candidate, 0)