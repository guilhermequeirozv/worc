from flask import Flask, request, jsonify
import db
from flask_cors import CORS, cross_origin
import os

app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = os.urandom(24)
CORS(app, resources={r'/*': {'origins': '*'}})

@app.route("/", methods=['POST'])
def root():
    return jsonify("hello worc")

@app.route("/candidates", methods=['GET', 'POST'])
@cross_origin(supports_credentials=True)
def all_candidates():
    response_object = {}
    if request.method == 'POST':
        post_data = request.get_json()
        candidate = {
            'name': post_data.get('name'),
            'sobrenome': post_data.get('sobrenome'),
            'document': post_data.get('document'),
            'type_document': post_data.get('type_document'),
            'job': post_data.get('job'),
            'contacts': post_data.get('contacts')
        }
        response_object = db.insert_candidate(candidate)
    elif request.method == 'GET':
        response_object['candidates'] = db.select_candidates()
    return jsonify(response_object)

@app.route("/candidates/<id>", methods=['GET', 'PUT', 'DELETE'])
@cross_origin(supports_credentials=True)
def candidate(id):
    response_object = {}
    if request.method == 'PUT':
        post_data = request.get_json()
        print(post_data)
        candidate = {
            'id': id,
            'name': post_data.get('name'),
            'sobrenome': post_data.get('sobrenome'),
            'document': post_data.get('document'),
            'type_document': post_data.get('type_document'),
            'job': post_data.get('job'),
            'contacts': post_data.get('contacts')
        }
        response_object = db.update_candidate(candidate)
    elif request.method == 'GET':
        response_object['candidate'] = db.select_candidate({'id': int(id)})
    elif request.method == 'DELETE':
        response_object = db.delete_candidate({'id': int(id)})
    return jsonify(response_object)

@app.route("/contacts", methods=['GET', 'POST'])
@cross_origin(supports_credentials=True)
def all_contacts():
    response_object = {}
    if request.method == 'POST':
        post_data = request.get_json()
        contacts = {
            'candidate_id': post_data.get('candidate_id'),
            'type': post_data.get('type'),
            'number': post_data.get('number')
        }
        response_object = db.insert_contact(contacts)
    elif request.method == 'GET':
        response_object['contacts'] = db.select_contacts()
    return jsonify(response_object)

@app.route("/contacts/<id>", methods=['GET', 'PUT', 'DELETE'])
@cross_origin(supports_credentials=True)
def contact(id):
    response_object = {}
    if request.method == 'PUT':
        post_data = request.get_json()
        contact = {
            'type': post_data.get('type'),
            'number': post_data.get('number'),
            'id': id,
        }
        response_object = db.update_contact(contact)
    elif request.method == 'GET':
        response_object['contact'] = db.select_contact({'id': id})
    elif request.method == 'DELETE':
        response_object = db.delete_contact({'id': id})
    return jsonify(response_object)

if __name__ == '__main__':    
    app.run()