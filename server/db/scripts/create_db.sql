DROP TABLE IF EXISTS CANDIDATES;
DROP TABLE IF EXISTS CONTACTS;

PRAGMA foreign_keys = ON;

CREATE TABLE "CANDIDATES" (
	"id" INTEGER,
	"name"	TEXT,
	"sobrenome"	TEXT,
	"document"	TEXT UNIQUE,
	"type_document"	TEXT,
	"job"	TEXT,
	"system_creation_date" DATE,
	"system_update_date" DATE,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "CONTACTS" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"candidate_id"	TEXT,
	"type" TEXT,
	"number" TEXT,
	"system_creation_date" DATE,
	"system_update_date" DATE,
	UNIQUE ("candidate_id","id"),
	FOREIGN KEY("candidate_id") REFERENCES CANDIDATES("id") ON UPDATE CASCADE ON DELETE CASCADE
);

